package hireright.ecxeption;

public class JsonConfigurationParseException extends RuntimeException {
    public JsonConfigurationParseException() {
    }

    public JsonConfigurationParseException(String message) {
        super(message);
    }

    public JsonConfigurationParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
