package hireright.common;

public class HandelResponse {

    private final int code;
    private final byte[] responseBody;

    public HandelResponse(int code, byte[] responseBody) {
        this.code = code;
        this.responseBody = responseBody;
    }

    public int getCode() {
        return code;
    }

    public byte[] getResponseBody() {
        return responseBody;
    }
}
