package hireright.common;

public class CompositeKey {

    public final String value;

    public CompositeKey(String... keys) {
        this.value = String.join("-", keys);
    }

}
