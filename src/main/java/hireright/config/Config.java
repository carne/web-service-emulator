package hireright.config;

import hireright.http.HttpMethod;

import java.util.Map;

public class Config {

    private String uri;
    private HttpMethod httpMethod;
    private RequestConfig request;
    private ResponseConfig response;
    private Map<String, String> storage;
    private long networkLatency;
    private boolean networkError;


    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }



    public Map<String, String> getStorage() {
        return storage;
    }

    public void setStorage(Map<String, String> storage) {
        this.storage = storage;
    }

    public RequestConfig getRequest() {
        return request;
    }

    public void setRequest(RequestConfig request) {
        this.request = request;
    }

    public ResponseConfig getResponse() {
        return response;
    }

    public void setResponse(ResponseConfig response) {
        this.response = response;
    }

    public long getNetworkLatency() {
        return networkLatency;
    }

    public void setNetworkLatency(long networkLatency) {
        this.networkLatency = networkLatency;
    }

    public boolean isNetworkError() {
        return networkError;
    }

    public void setNetworkError(boolean networkError) {
        this.networkError = networkError;
    }
}
