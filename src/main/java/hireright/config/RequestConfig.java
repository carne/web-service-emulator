package hireright.config;

import hireright.http.HttpDataType;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;

public class RequestConfig {
    private HttpDataType type;

    private String expectedFields;

    public HttpDataType getType() {
        return type;
    }

    public void setType(HttpDataType type) {
        this.type = type;
    }

    public List<String> getExpectedFields() {
        return isNull(expectedFields) || expectedFields.isEmpty() ? emptyList(): asList(expectedFields.split(","));
    }

    public void setExpectedFields(String expectedFields) {
        this.expectedFields = expectedFields;
    }

}
