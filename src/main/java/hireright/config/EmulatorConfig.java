package hireright.config;

import java.util.List;

public class EmulatorConfig {

    private List<Config> configs;

    public List<Config> getConfigs() {
        return configs;
    }

    public void setConfigs(List<Config> configs) {
        this.configs = configs;
    }

}
