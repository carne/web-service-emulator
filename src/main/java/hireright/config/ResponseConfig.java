package hireright.config;

import hireright.http.HttpDataType;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;

public class ResponseConfig {
    private HttpDataType type;
    private String returnFields;

    public HttpDataType getType() {
        return type;
    }

    public void setType(HttpDataType type) {
        this.type = type;
    }

    public List<String> getReturnFields() {
        return isNull(returnFields) || returnFields.isEmpty() ? emptyList(): asList(returnFields.split(","));
    }

    public void setReturnFields(String returnFields) {
        this.returnFields = returnFields;
    }

}
