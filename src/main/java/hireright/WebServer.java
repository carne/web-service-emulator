package hireright;

import com.sun.net.httpserver.HttpServer;
import hireright.config.EmulatorConfig;
import hireright.handler.*;
import hireright.http.HttpDataType;
import hireright.reader.JsonConfigurationParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class WebServer {

    private static final Logger logger = LoggerFactory.getLogger(WebServer.class.getSimpleName());

    public static void main(String[] args) throws IOException {

        if (args.length < 1) {
            throw new RuntimeException("not passed the path to the configuration file");
        }
        JsonConfigurationParser jsonConfigurationParser = new JsonConfigurationParser();
        EmulatorConfig config = jsonConfigurationParser.parse(args[0], EmulatorConfig.class);

        HttpServer httpServer = HttpServer.create(new InetSocketAddress(8080), 0);
        httpServer.createContext("/", new MainHttpExchangeHandler(config, getRequestHandlers(), getResponseHandlers()));
        httpServer.start();
        logger.info("web server is running");
    }

    private static Map<HttpDataType, ResponseHandler> getResponseHandlers() {
        return Stream.of(new JsonResponseHandler(), new XmlResponseHandler())
                .collect(toMap(ResponseHandler::getType, Function.identity()));
    }

    private static Map<HttpDataType, RequestHandler> getRequestHandlers() {
        return Stream.of(new JsonRequestHandler(), new QueryRequestHandler(), new XmlRequestHandler())
                .collect(toMap(RequestHandler::getType, Function.identity()));
    }
}
