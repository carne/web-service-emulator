package hireright.reader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hireright.ecxeption.JsonConfigurationParseException;

import java.io.File;
import java.io.FileReader;

public class JsonConfigurationParser {
    private Gson gson = new  GsonBuilder().serializeNulls().create();

    public <T> T parse(String path, Class<T> toClass) {
       FileReader fileReader;

        try {
            File file = new File(path);
            fileReader = new FileReader(file);
        } catch (Exception e) {
            throw new JsonConfigurationParseException(String.format("Not found file by Path %s ", path));
        }
        try {
            return gson.fromJson(fileReader, toClass);
        } catch (Exception e) {
            throw new JsonConfigurationParseException("can't transform file to json");
        }
    }

}
