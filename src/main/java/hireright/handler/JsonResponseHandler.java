package hireright.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hireright.http.HttpDataType;

import java.util.Map;

public class JsonResponseHandler extends AbstractResponseHandler {
    private final Gson gson = new GsonBuilder().create();

    @Override
    public HttpDataType getType() {
        return HttpDataType.JSON;
    }


    @Override
    protected byte[] serialize(Map<String, String> responseBody) {
        String json = gson.toJson(responseBody);
        return json.getBytes();
    }

}
