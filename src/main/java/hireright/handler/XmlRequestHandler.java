package hireright.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sun.net.httpserver.HttpExchange;
import hireright.common.CompositeKey;
import hireright.http.HttpDataType;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class XmlRequestHandler implements RequestHandler {
    @Override
    public HttpDataType getType() {
        return HttpDataType.XML;
    }

    @Override
    public CompositeKey handle(List<String> fields, HttpExchange httpExchange) {
        XmlMapper mapper = new XmlMapper();
        try {
            Map<String, String> xmlMap = mapper.readValue(new InputStreamReader(httpExchange.getRequestBody()), new TypeReference<>() {});
            String joinedXmlValue = fields.stream().map(xmlMap::get).filter(Objects::nonNull).collect(Collectors.joining("-"));
            return new CompositeKey(joinedXmlValue);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
