package hireright.handler;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import hireright.common.CompositeKey;
import hireright.common.HandelResponse;
import hireright.config.Config;
import hireright.config.EmulatorConfig;
import hireright.http.HttpDataType;
import hireright.http.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

public class MainHttpExchangeHandler implements HttpHandler {
    private static final Logger logger = LoggerFactory.getLogger(MainHttpExchangeHandler.class.getSimpleName());


    private final Map<String, Config> configs;
    private final Map<HttpDataType, RequestHandler> requestHandlers;
    private final Map<HttpDataType, ResponseHandler> responseHandlers;

    public MainHttpExchangeHandler(EmulatorConfig emulatorConfig, Map<HttpDataType, RequestHandler> requestHandlers, Map<HttpDataType, ResponseHandler> responseHandlers) {
        configs = emulatorConfig.getConfigs().stream().collect(toMap(Config::getUri, Function.identity()));

        this.requestHandlers = requestHandlers;
        this.responseHandlers = responseHandlers;
    }


    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        loggingHeaders(httpExchange.getRequestHeaders(), "Request headers: ");
        Config config = configs.get(httpExchange.getRequestURI().getPath());
        if (isNull(config)) {
            sendResponse(404, "Page not found", httpExchange);
            return;
        }
        emulateNetworkLatency(config.getNetworkLatency());
        if (config.isNetworkError()) {
            sendResponse(500, "Server error", httpExchange);
            return;
        }
        try {
            HandelResponse response = handelRequest(httpExchange, config);
            addContentType(httpExchange, config.getResponse().getType());
            loggingHeaders(httpExchange.getResponseHeaders(), "response headers: ");
            loggingHttpBody(response.getResponseBody(), "response");
            sendResponse(response.getCode(), response.getResponseBody(), httpExchange);
        } catch (Exception e) {
            logger.error("error handler request: ", e);
            sendResponse(500, e.toString(), httpExchange);
        }

    }

    private void emulateNetworkLatency(long networkLatency) {
        if (networkLatency > 0) {
            try {
                Thread.sleep(networkLatency);
            } catch (InterruptedException e) {

            }
        }
    }

    private List<String> addContentType(HttpExchange httpExchange, HttpDataType httpDataType) {
        return httpExchange.getResponseHeaders().put("Content-Type", Collections.singletonList(httpDataType.contentType));
    }

    private void sendResponse(int code, String body, HttpExchange exchange) throws IOException {
        sendResponse(code, body.getBytes(), exchange);
    }


    private void sendResponse(int code, byte[] body, HttpExchange exchange) throws IOException {
        exchange.sendResponseHeaders(code, body.length);
        OutputStream os = exchange.getResponseBody();
        os.write(body);
        os.close();
    }

    private HandelResponse handelRequest(HttpExchange httpExchange, Config config) {

        HttpMethod httpMethod = HttpMethod.getByStringName(httpExchange.getRequestMethod()).orElse(null);
        if (!config.getHttpMethod().equals(httpMethod)) {
            return new HandelResponse(405, "Method Not Allowed".getBytes());
        }

        RequestHandler requestHandler = requestHandlers.get(config.getRequest().getType());
        if (isNull(requestHandler)) {
            return new HandelResponse(400, "not found handler for request".getBytes());
        }
        ResponseHandler responseHandler = responseHandlers.get(config.getResponse().getType());
        if (isNull(responseHandler)) {
            return new HandelResponse(400, "not found handler for response".getBytes());
        }

        CompositeKey compositeKey = requestHandler.handle(config.getRequest().getExpectedFields(), httpExchange);
        byte[] bodyResponse = responseHandler.getBodyResponse(config.getResponse().getReturnFields(), compositeKey, config.getStorage());
        return new HandelResponse(200, bodyResponse);

    }

    private void loggingHttpBody(byte[] body, String message) {
        if (logger.isDebugEnabled()) {
            String bodyAsString = message + " " + new String(body);
            logger.debug(bodyAsString);
        }
    }

    private void loggingHeaders(Map<String, List<String>> headers, String message) {
        if (logger.isDebugEnabled()) {
            String mapAsString = headers.entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + ": " + String.join(",", entry.getValue()))
                    .collect(joining("\n"));

            logger.debug("{} {}", message, mapAsString);
        }
    }
}
