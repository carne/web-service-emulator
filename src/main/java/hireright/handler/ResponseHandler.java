package hireright.handler;

import hireright.common.CompositeKey;
import hireright.http.HttpDataType;

import java.util.List;
import java.util.Map;

public interface ResponseHandler {
    HttpDataType getType();

    byte[] getBodyResponse(List<String> returnFields, CompositeKey compositeKey, Map<String, String> storage);
}
