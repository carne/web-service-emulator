package hireright.handler;

import com.sun.net.httpserver.HttpExchange;
import hireright.common.CompositeKey;
import hireright.http.HttpDataType;
import hireright.http.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.joining;

public class QueryRequestHandler implements RequestHandler {
    private static final Logger logger = LoggerFactory.getLogger(QueryRequestHandler.class.getSimpleName());

    @Override
    public HttpDataType getType() {
        return HttpDataType.QUERY;
    }

    @Override
    public CompositeKey handle(List<String> fields, HttpExchange httpExchange) {
        logUri(httpExchange.getRequestURI());
        String queriesValues = fields.stream()
                .map(field -> HttpUtils.getQueryValue(httpExchange.getRequestURI().getQuery(), field))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(joining("-"));

        return new CompositeKey(queriesValues);
    }

    private void logUri(URI requestURI) {
        if (logger.isDebugEnabled()) {
            logger.debug(requestURI.toString());
        }
    }

}
