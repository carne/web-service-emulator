package hireright.handler;

import com.sun.net.httpserver.HttpExchange;
import hireright.common.CompositeKey;
import hireright.http.HttpDataType;

import java.util.List;

public interface RequestHandler {
    HttpDataType getType();

    CompositeKey handle(List<String> fields, HttpExchange httpExchange);
}
