package hireright.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.net.httpserver.HttpExchange;
import hireright.common.CompositeKey;
import hireright.http.HttpDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class JsonRequestHandler implements RequestHandler {
    private static final Logger logger = LoggerFactory.getLogger(JsonRequestHandler.class.getSimpleName());
    private final Gson gson = new GsonBuilder().create();
    @Override
    public HttpDataType getType() {
        return HttpDataType.JSON;
    }

    @Override
    public CompositeKey handle(List<String> fields, HttpExchange httpExchange) {
        InputStream requestBody = logger.isDebugEnabled()? logRequest(httpExchange.getRequestBody()): httpExchange.getRequestBody();
        Map<String, String> jsonAsMap = gson.fromJson(new InputStreamReader(requestBody), new TypeToken<Map<String, String>>(){}.getType());
        String joinedJsonValue = fields.stream().map(jsonAsMap::get).filter(Objects::nonNull).collect(Collectors.joining("-"));
        return new CompositeKey(joinedJsonValue);
    }

    private InputStream logRequest(InputStream requestBody) {
        try {
            byte[] bytes = requestBody.readAllBytes();
            logger.debug("request body" + new String(bytes));
            return new ByteArrayInputStream(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
