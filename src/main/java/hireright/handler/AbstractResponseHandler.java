package hireright.handler;

import hireright.common.CompositeKey;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;

public abstract class AbstractResponseHandler implements ResponseHandler {

    @Override
    public byte[] getBodyResponse(List<String> returnFields, CompositeKey compositeKey, Map<String, String> storage) {
        String storageData = storage.get(compositeKey.value);
        List<String> listValue = isNull(storageData) || storageData.isEmpty() ? emptyList() : List.of(storageData.split(","));
        Map<String, String> responseBody = new HashMap<>();
        for (int i = 0; i < returnFields.size(); i++) {
            responseBody.put(returnFields.get(i), getValue(listValue, i));
        }
        return serialize(responseBody);

    }

    protected abstract byte[] serialize(Map<String, String> responseBody);

    private String getValue(List<String> listValue, int i) {
        if (i >= listValue.size()) {
            return "";
        }
        return listValue.get(i);
    }
}
