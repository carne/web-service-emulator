package hireright.handler;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import hireright.http.HttpDataType;

import java.util.Map;

public class XmlResponseHandler extends AbstractResponseHandler {
    private final XmlMapper mapper = new XmlMapper();

    @Override
    public HttpDataType getType() {
        return HttpDataType.XML;
    }


    @Override
    protected byte[] serialize(Map<String, String> responseBody) {
        try {
            return mapper.writeValueAsBytes(responseBody);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}
