package hireright.http;

import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Objects.isNull;

public enum HttpMethod {
    POST, GET;

    public static Optional<HttpMethod> getByStringName(String name) {
        if (isNull(name)) {
            return Optional.empty();
        }
        return Stream.of(HttpMethod.values())
                .filter(httpMethod -> httpMethod.name().equals(name.toUpperCase()))
                .findFirst();
    }
}
