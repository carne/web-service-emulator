package hireright.http;

import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Objects.isNull;

public final class HttpUtils {


    private HttpUtils() {
    }

    public static Optional<String> getQueryValue(String rowQueries, String queryName) {
        if (isNull(rowQueries) || rowQueries.isEmpty()) {
            return Optional.empty();
        }
        return Stream.of(rowQueries.split("&"))
                .map(s -> s.split("="))
                .filter(array -> array.length == 2 && queryName.equals(array[0]))
                .map(array -> array[1])
                .findFirst();
    }

}
