package hireright.http;

public enum HttpDataType {
    JSON("application/json"), XML("application/xml"), QUERY("");
    public final String contentType;
    HttpDataType(String contentType) {
        this.contentType = contentType;
    }


}
