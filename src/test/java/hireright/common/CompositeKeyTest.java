package hireright.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CompositeKeyTest {

    @Test
    public void getKey() {
        CompositeKey compositeKey = new CompositeKey("one", "two");
        assertEquals("one-two", compositeKey.value);
    }

}