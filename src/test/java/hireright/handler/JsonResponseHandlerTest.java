package hireright.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hireright.common.CompositeKey;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonResponseHandlerTest {

    @Test
    public void getBodyResponse() {
        Gson gson = new GsonBuilder().create();
        JsonResponseHandler responseHandler = new JsonResponseHandler();
        List<String> returnFields = List.of("one", "two", "three");
        Map<String, String> storage = Collections.singletonMap("composite-key", "1,2,3");
        CompositeKey compositeKey = new CompositeKey("composite-key");

        byte[] bodyResponse = responseHandler.getBodyResponse(returnFields, compositeKey, storage);
        Map<String, String> map = gson.fromJson(new String(bodyResponse), Map.class);

        assertEquals("1", map.get("one"));
        assertEquals("2", map.get("two"));
        assertEquals("3", map.get("three"));


    }

}