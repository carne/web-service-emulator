package hireright.handler;

import com.sun.net.httpserver.HttpExchange;
import hireright.common.CompositeKey;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class JsonRequestHandlerTest {
    private final static String JSON = "{\"id\": \"1\", \"name\": \"ivan\"}";
    @Test
    public void handle() {
        InputStream inputStream = new ByteArrayInputStream(JSON.getBytes());
        JsonRequestHandler jsonRequestHandler = new JsonRequestHandler();

        List<String> list = new ArrayList<>(Arrays.asList("id", "name"));
        HttpExchange exchange = mock(HttpExchange.class);
        when(exchange.getRequestBody()).thenReturn(inputStream);
        CompositeKey key = jsonRequestHandler.handle(list, exchange);
        assertEquals("1-ivan", key.value);
    }


}