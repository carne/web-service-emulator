package hireright.handler;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import hireright.common.CompositeKey;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class XmlResponseHandlerTest {

    @Test
    public void getBodyResponse () throws IOException {
        XmlMapper mapper = new XmlMapper();
        XmlResponseHandler xmlResponseHandler = new XmlResponseHandler();
        byte[] bodyResponse = xmlResponseHandler.getBodyResponse(Collections.singletonList("one"), new CompositeKey("one"), Collections.singletonMap("one", "1"));
        Map<String, String> map = mapper.readValue(bodyResponse, Map.class);
        assertEquals("1", map.get("one"));

    }

}