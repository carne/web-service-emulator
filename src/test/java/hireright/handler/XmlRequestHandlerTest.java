package hireright.handler;

import com.sun.net.httpserver.HttpExchange;
import hireright.common.CompositeKey;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class XmlRequestHandlerTest {
    private final static String XML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
            "<xml>\n" +
            " <id>1</id>\n" +
            " <name>ivan</name>\n" +
            "</xml>";
    @Test
    public void handle() {
        InputStream inputStream = new ByteArrayInputStream(XML.getBytes());
        XmlRequestHandler xmlRequestHandler = new XmlRequestHandler();

        List<String> list = new ArrayList<>(Arrays.asList("id", "name"));
        HttpExchange exchange = mock(HttpExchange.class);
        when(exchange.getRequestBody()).thenReturn(inputStream);
        CompositeKey key = xmlRequestHandler.handle(list, exchange);
        assertEquals("1-ivan", key.value);
    }

}