package hireright.http;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class HttpUtilsTest {

    @Test
    public void getQueryValue() {
        Optional<String> id = HttpUtils.getQueryValue("name=john&id=432", "id");
        assertTrue(id.isPresent());
        assertEquals("432", id.get());
    }

}