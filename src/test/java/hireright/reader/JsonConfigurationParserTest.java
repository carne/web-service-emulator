package hireright.reader;

import hireright.config.EmulatorConfig;
import hireright.ecxeption.JsonConfigurationParseException;
import hireright.http.HttpDataType;
import hireright.http.HttpMethod;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import hireright.config.Config;
import static org.junit.jupiter.api.Assertions.*;

public class JsonConfigurationParserTest {
    private Path path = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource("emulatroServiceConfig.json")).getPath());
    private final JsonConfigurationParser jsonConfigurationParser = new JsonConfigurationParser();

    @Test
    public void  loadFileAndConvertToEmulatorConfig() {

        EmulatorConfig emulatorConfig = jsonConfigurationParser.parse(path.toString(), EmulatorConfig.class);
        assertNotNull(emulatorConfig);
        List<Config> configs = emulatorConfig.getConfigs();
        assertEquals(2, configs.size());

        Config config1 = configs.get(0);
        assertNotNull(config1);
        assertEquals("uri/1", config1.getUri());
        assertEquals(HttpMethod.POST, config1.getHttpMethod());
        assertEquals(HttpDataType.XML, config1.getRequest().getType());
        assertEquals(HttpDataType.JSON, config1.getResponse().getType());
        assertEquals(2, config1.getStorage().size());


        Config config2 = configs.get(1);
        assertNotNull(config2);
        assertEquals("uri/2", config2.getUri());
        assertEquals(HttpMethod.GET, config2.getHttpMethod());
        assertEquals(HttpDataType.QUERY, config2.getRequest().getType());

//        assertEquals(HttpDataType.JSON, config2.getRequestDataType());

    }

    @Test
    public void whenNotFoundFile() {
        assertThrows(JsonConfigurationParseException.class, () -> jsonConfigurationParser.parse("/test.json", TestJson.class));
    }

    private static class TestJson{
        private String value;
        private String value2;
    }


}