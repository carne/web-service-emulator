Step for run project 
1. mvn clean install
2. java -jar web-service-emulator-1.0-SNAPSHOT.jar /pathToConfigServiceFile/emulatroServiceConfig.json

In the resource directory (web-service-emulator/src/main/resources/emulatroServiceConfig.json) there is config file with configuration of two services

location service
curl -v -d '{"zip":"10111"}' -H "Content-Type: application/json" -X POST http://localhost:8080/location

weather service
curl -v  -X GET http://localhost:8080/weather?cityCode=1